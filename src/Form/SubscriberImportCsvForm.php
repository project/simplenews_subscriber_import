<?php

namespace Drupal\simplenews_subscriber_import\Form;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\simplenews\Entity\Subscriber;
use Drupal\simplenews\Form\SubscriberMassSubscribeForm;
use Drupal\simplenews\SubscriberInterface;
use Drupal\simplenews\Subscription\SubscriptionManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Simplenews Subscriber Import form.
 */
class SubscriberImportCsvForm extends SubscriberMassSubscribeForm {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The import controller.
   *
   * @var \Drupal\Core\Controller\ControllerBase
   */
  protected $importController;

  /**
   * {@inheritdoc}
   */
  public function __construct(LanguageManagerInterface $language_manager, SubscriptionManagerInterface $subscription_manager, EmailValidatorInterface $email_validator, EntityTypeManagerInterface $entity_type_manager, ControllerBase $import_controller) {
    parent::__construct($language_manager, $subscription_manager, $email_validator);
    $this->entityTypeManager = $entity_type_manager;
    $this->importController = $import_controller;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
      $container->get('simplenews.subscription_manager'),
      $container->get('email.validator'),
      $container->get('entity_type.manager'),
      $container->get('simplenews_subscriber_import_controller')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simplenews_subscriber_import_subscriber_import_csv';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    if (isset($form['emails'])) {
      unset($form['emails']);
    }

    $form['info'] = [
      '#type' => 'details',
      '#title' => $this->t('Explanation and blueprint'),
      '#open' => TRUE,
      '#weight' => -2,
    ];

    $form['info']['text']['#markup'] = $this->t('
      This form allows you to upload a CSV file which will be used for importing
      subscribers. The first line of the file (headers) must contain a "mail"
      header and all the config field names of the Subscriber entity. You can
      download a blueprint by clicking on the button below to make sure that you
      use the correct format.
    ');

    $blueprintLink = Link::createFromRoute(
      $this->t('Download CSV blueprint'),
      'simplenews_subscriber_import.subscriber_import_csv_blueprint',
      [],
      [
        'attributes' => [
          'class' => ['button', 'button--small'],
        ],
      ]
    );

    $form['info']['blueprint'] = [
      '#type' => 'container',
      'link' => $blueprintLink->toRenderable(),
    ];

    $form['file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('File'),
      '#upload_location' => 'temporary://simplenews-subscriber-import',
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
      '#required' => TRUE,
      '#weight' => -1,
      '#description' => $this->t('Upload the CSV file that contains the subscriber data.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $file = $this->getFile($form_state);

    $csv_is_valid = $this->importController->validateCsv($file);

    if (!$csv_is_valid) {
      $form_state->setErrorByName('file', $this->t('The file is not valid.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file = $this->getFile($form_state);
    $data = $this->importController->parseCsv($file);

    // Early return if there's nothing to import.
    if (empty($data)) {
      return;
    }

    $emails = array_column($data, 'mail');
    $form_state->setValue('emails', implode("\n", $emails));

    // Submit the parent form which handles all the logic for subscribing.
    parent::submitForm($form, $form_state);

    // Setting up the batch.
    $batch = [
      'title' => $this->t('Importing field values for subscribers...'),
      'operations' => [],
    ];

    // Fill the batch.
    foreach ($data as $values) {
      $batch['operations'][] = [
        __CLASS__ . '::importFieldValues', [$values],
      ];
    }

    // Deletes the file before starting the batch, because the data is already
    // pushed to the batch, and we don't want (and need) to keep this file if
    // the batch fails for example.
    //
    // The file should also be deleted if it's uploaded but the form is not
    // submitted. See https://www.drupal.org/project/drupal/issues/3282721 for
    // more information about this issue.
    $file->delete();

    // Executes the batch.
    batch_set($batch);
  }

  /**
   * Updates the field values for a single subscriber entity.
   *
   * @param array $values
   *   The field values.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function importFieldValues(array $values): void {
    $email = $values['mail'];
    $subscriber = Subscriber::loadByMail($email);

    // Early return if the subscriber does not exist.
    if (!$subscriber instanceof SubscriberInterface) {
      return;
    }

    foreach ($values as $field => $value) {
      $subscriber->set($field, $value);
    }

    $subscriber->save();
  }

  /**
   * Loads the file that is uploaded in the form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Returns the file entity or null if it does not exist.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getFile(FormStateInterface $form_state): ?EntityInterface {
    $value = $form_state->getValue('file');
    $fid = reset($value);

    return $this->entityTypeManager
      ->getStorage('file')
      ->load($fid);
  }

}
