<?php

namespace Drupal\simplenews_subscriber_import\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for Simplenews Subscriber Import routes.
 */
class SimplenewsSubscriberImportController extends ControllerBase {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * Creates an array with the first row column headers as the keys.
   *
   * See https://www.php.net/manual/en/function.str-getcsv.php#117692
   *
   * @param \Drupal\Core\Entity\EntityInterface $file
   *   A file entity containing the CSV file.
   *
   * @return array
   *   An array with the data from the CSV file.
   */
  public function parseCsv(EntityInterface $file): array {
    $csv = array_map('str_getcsv', file($file->getFileUri()));

    array_walk($csv, function (&$a) use ($csv) {
      $a = array_combine($csv[0], $a);
    });

    array_shift($csv);

    return $csv;
  }

  /**
   * Validates if the CSV is in the correct format.
   *
   * @param \Drupal\Core\Entity\EntityInterface $file
   *   A file entity containing the CSV file.
   *
   * @return bool
   *   Whether the file entity contains a valid CSV for subscriber importing.
   */
  public function validateCsv(EntityInterface $file): bool {
    $fileUri = $file->getFileUri();
    $firstLine = file($fileUri)[0] ?? NULL;

    // Early return if there is no first line.
    if (is_null($firstLine)) {
      return FALSE;
    }

    $csvHeaders = str_getcsv($firstLine);
    $subscriberFields = array_keys($this->getSubscriberFields());

    // Check if the header of the CSV matches the subscriber fields.
    if (sort($csvHeaders) !== sort($subscriberFields)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns a blueprint CSV file with the subscriber fields as header.
   */
  public function csvBlueprint(): Response {
    $subscriberFields = $this->getSubscriberFields();

    $csvColumns = array_keys($subscriberFields);

    $csv = fopen('php://output', 'r+');
    fputcsv($csv, $csvColumns);

    $output = stream_get_contents($csv);

    $response = new Response();

    // By setting these 2 header options, the browser will return the CSV file.
    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Content-Disposition', 'attachment; filename="blueprint-simplenews-subscriber-import.csv"');

    $response->setContent($output);

    return $response;
  }

  /**
   * Returns the config fields (and the mail field) of the subscriber entity.
   *
   * @return array
   *   An array with subscriber fields.
   */
  protected function getSubscriberFields(): array {
    $fieldDefinitions = $this->entityFieldManager
      ->getFieldStorageDefinitions('simplenews_subscriber');

    $configFields = array_filter($fieldDefinitions, function ($fieldDefinition) {
      return !$fieldDefinition->isBaseField();
    });

    return array_merge(['mail' => $fieldDefinitions['mail']], $configFields);
  }

}
